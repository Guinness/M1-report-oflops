default:
	pdflatex -shell-escape report.tex
	bibtex *.aux
	pdflatex -shell-escape report.tex
	makeglossaries report
	pdflatex -shell-escape report.tex
